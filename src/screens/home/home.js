// react imports
import React, { useEffect, useState } from 'react'

// rrd imports
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'

// home imports
import './home.css'

// screen imports
import Library from '../library/library'
import Feed from '../feed/feed'
import Trending from '../trending/trending'
import Player from '../player/player'
import Favorites from '../favorites/favorites'
import Login from '../auth/login'

// component imports
import Sidebar from '../../components/sidebar/sidebar'
import { setClientToken } from '../../spotify'

export default function Home() {
    const [token, setToken] = useState('');

    useEffect(() => {
        const token = window.localStorage.getItem('token');
        const hash = window.location.hash;
        window.location.hash = '';
        if (!token && hash) {
            const _token = hash.split('&')[0].split('=')[1];
            window.localStorage.setItem("token", _token);
            setToken(_token);
            setClientToken(_token);
        } else {
            setToken(token);
        }
    }, [])

    return !token ? (
        <Login />
    ) : (
        <Router>
            <div className="main-body">
                <Sidebar />
                <Routes>
                    <Route path="/" element={<Library />} />
                    <Route path="/feed" element={<Feed />} />
                    <Route path="/trending" element={<Trending />} />
                    <Route path="/player" element={<Player />} />
                    <Route path="/favorites" element={<Favorites />} />
                </Routes>
            </div>
        </Router>
    )
}
