import React, { useEffect, useState } from 'react'

// react icon imports
import { MdFavorite } from 'react-icons/md'
import { MdSpaceDashboard } from 'react-icons/md'
import { FaGripfire, FaPlay } from 'react-icons/fa'
import { FaSignOutAlt } from 'react-icons/fa'
import { IoLibrary } from 'react-icons/io5'


import './sidebar.css'
import SidebarButton from './sidebarButton'
import apiClient from '../../spotify'

export default function Sidebar() {
    const [image, setImage] = useState(
        "https://cdn.discordapp.com/attachments/1054958023698825266/1116997594464387132/Mizuki_sailor.png"
    );

    useEffect(() => {
        apiClient.get("me").then((response) => {
            setImage(response.data.images[0].url);
        })
    }, []);

    return (
        <div className='sidebar-container'>
            <img
                src={image}
                className='profile-img'
                alt='profile'
            />
            <div>
                <SidebarButton title="Feed" to="/feed" icon={<MdSpaceDashboard />} />
                <SidebarButton title="Trending" to="/trending" icon={<FaGripfire />} />
                <SidebarButton title="Player" to="/player" icon={<FaPlay />} />
                <SidebarButton title="Favorites" to="/favorites" icon={<MdFavorite />} />
                <SidebarButton title="Library" to="/" icon={<IoLibrary />} />
            </div>
            <SidebarButton title="Sign Out" to="" icon={<FaSignOutAlt />} />
        </div>
    )
}
